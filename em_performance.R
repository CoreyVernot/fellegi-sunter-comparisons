
#' Run EM algorithm to estimate weights
#'
#' Creates a data frame with the weights for every comparison value. Weights are post-blocking / indexing
#' @param des the design matrix with all of the unique values of the comparison values
#' @param counts  the frequency of each set of comparisons appearing in the datasets
#' @param piM vector of initial values for the conditional probabilities of observing comparisons given matching
#' @param piU vector initial values for the conditional probabilities of observing comparisons given non-matching
#' @param pM initial value for the proportion of matching pairs
#' @keywords EM
#' @export
#' @examples
#' library(RecordLinkage, quietly = T)
#' data("RLdata10000")
#' dat = RLdata10000
#' dat$fi = substr(dat$fname_c1, 1,1)
#' dat$li = substr(dat$lname_c1, 1,1)
#' block_cols = c("fi", "li")
#' comp_cols = c("fname_c1" ,"lname_c1" ,"by" , "bm", "bd")
#' processed = makePairsDedup(dat, comp_cols, block_cols = block_cols, ids = NA)
#' counts = processed$counts
#' des = processed$design
#' piM = 0.95^rowSums(des != "0")*0.05^(ncol(des)- rowSums(des != "0"))
#' piM = piM/sum(piM)
#' em = fsEM(des = processed$design, counts = processed$counts, 
#'          piM = (counts)/sum(counts), pM = 0.1, maxiter = 10000, tol = 1e-6)

##    Outputs:  List with 2 components:
##                weights- A data frame of the design matrix with the estimated marginal probabilities and 
##                weights appended as additional columns.

fsEM = function(des, counts, piM, piU, pM, maxiter, tol){
  n = sum(counts)
  for(i in 1:maxiter) {
    # E step
    cprobM = (1-pM)*piU/((1-pM)*piU + pM*piM)
    nU = counts*cprobM
    nM = counts*(1-cprobM)
    
    # M step
    glm.fit.0 = glm(y~., data=data.frame(y=nU, des),
                    family="quasipoisson")
    glm.fit.1 = glm(y~., data=data.frame(y=nM, des),
                    family="quasipoisson")
    
    pM = sum(nM)/n
    
    
    piU.old = piU
    piM.old = piM
    
    g = function(fit) {
      logwt = predict(fit)
      logwt = logwt - max(logwt)
      wt = exp(logwt)
      wt/sum(wt)
    }
    piU = g(glm.fit.0)
    piM = g(glm.fit.1)
    if (max(abs(log(piM/piU) - log(piM.old/piU.old))) < tol) break
  }
  
  df = data.frame(des, piU = piU, piM = piM)
  df$w = piM/piU
  df$Freq = counts
  return(list(weights = df, pM = pM))
}

#' Evaluate the performance of the matching
#' 
#' Calculates recall, precision, and true type I / type II error rates
#' @param pairs pairs that are created by makePairsDedup. Need to have ids
#' @param weights data frame of weights that are created by fsEM
#' @param match_col column name for column indicating whether a pair is a match or not.
#' @param comp_cols column names for columns used in the matching
#' @param pM Unconditional probability that a pair is a match, output by fsEM
#' @keywords performance
#' @export
#' @examples 
#' library(RecordLinkage, quietly = T)
#' data("RLdata10000")
#' dat = RLdata10000
#' dat$fi = substr(dat$fname_c1, 1,1)
#' dat$li = substr(dat$lname_c1, 1,1)
#' block_cols = c("fi", "li")
#' comp_cols = c("fname_c1" ,"lname_c1" ,"by" , "bm", "bd")
#' processed = makePairsDedup(dat, comp_cols, block_cols = block_cols, ids = identity.RLdata10000)
#' counts = processed$counts
#' des = processed$design
#' piM = 0.95^rowSums(des != "0")*0.05^(ncol(des)- rowSums(des != "0"))
#' piM = piM/sum(piM)
#' em = fsEM(des = processed$design, counts = processed$counts, 
#'          piM = (counts)/sum(counts), pM = 0.1, maxiter = 10000, tol = 1e-6)
#' performance = fsPerformance(pairs = processed$pairs, weights = em$weights, 
#'                             match_col = "is_match", comp_cols = comp_cols, pM = em$pM)

fsPerformance = function(pairs, weights, match_col, comp_cols, pM ){
  library(dplyr)
  df = weights
  colnames(pairs)[colnames(pairs) == match_col] = "is_match"
  pMReal = mean(pairs$is_match)
  pUReal = 1-pMReal
  nM = sum(pairs$is_match)
  nU = sum(1-pairs$is_match)
  
  groups = paste(comp_cols, collapse = ",")
  do = paste("weightActual = pairs %>% group_by(", groups, ") %>% 
             summarize(piMActual = sum(is_match)/nM, piUActual = sum(1-is_match)/nU)")
  eval(parse(text = do))
  df = merge(df, weightActual, by = comp_cols)
  df$w_star = df$piMActual/df$piUActual
  
  mu = lam = recall = prec = mu_star = lam_star = recall_star = prec_star = rep(NA, nrow(df))
  for(i in 1:nrow(df)){
    df = df[order(df$w), ]
    space_u = i >= 1:nrow(df)
    space_m = 1:nrow(df) > i
    lam[i] = sum(df$piM[space_u])
    mu[i] = sum(df$piU[space_m])
    recall[i] = sum(df$piM[space_m])
    prec[i] = recall[i]*pM/(recall[i]*pM + mu[i]*(1-pM))
    
    #### Doing the matching using actual weights = what is the predicted values of lamda (given weights) vs. the true values
    df = df[order(df$w_star), ]
    lam_star[i] = sum(df$piMActual[space_u])
    mu_star[i] = sum(df$piUActual[space_m])
    recall_star[i] = sum(df$piMActual[space_m])
    prec_star[i] = recall_star[i]*pMReal/(recall_star[i]*pMReal + mu_star[i]*(1-pMReal))
  }
  df = data.frame(df, lam = lam, mu =mu, lam_star = lam_star, mu_star = mu_star)
  
  pairs <- merge(pairs, df, by = comp_cols, all = T)
  
  false_pos = miss_rate = accuracy = tp = fp = tn = fn = 
    false_pos_star = miss_rate_star = accuracy_star = tp_star = fp_star = tn_star = fn_star = rep(NA, nrow(df))
  for( i in 1:nrow(df)){
    df = df[order(df$w), ]
    pairs$matchGuess = pairs$w >= df$w[i]
    accuracy[1] = mean(pairs$is_match == pairs$matchGuess)
    false_pos[i] = mean(pairs$is_match[pairs$matchGuess == 1] == 0)
    miss_rate[i] = mean(pairs$matchGuess[pairs$is_match == 1] == 0)
    tp[i] = sum(pairs$matchGuess == 1 & pairs$is_match == 1)
    fp[i] = sum(pairs$matchGuess == 1 & pairs$is_match == 0)
    tn[i] = sum(pairs$matchGuess == 0 & pairs$is_match == 0)
    fn[i] = sum(pairs$matchGuess == 0 & pairs$is_match == 1)
    
    df = df[order(df$w_star), ]
    pairs$matchGuess = pairs$w_star >=df$w_star[i]
    accuracy_star[i] = mean(pairs$is_match == pairs$matchGuess)
    false_pos_star[i] = mean(pairs$is_match[pairs$matchGuess == 1] == 0)
    miss_rate_star[i] = mean(pairs$matchGuess[pairs$is_match == 1] == 0)
    tp_star[i] = sum(pairs$matchGuess == 1 & pairs$is_match == 1)
    fp_star[i] = sum(pairs$matchGuess == 1 & pairs$is_match == 0)
    tn_star[i] = sum(pairs$matchGuess == 0 & pairs$is_match == 0)
    fn_star[i] = sum(pairs$matchGuess == 0 & pairs$is_match == 1)
  }
  
  results = data.frame(accuracy = accuracy, false_pos = false_pos, miss_rate = miss_rate, mu = mu, lam = lam,
                       tp = tp, fp = fp, tn = tn, fn = fn, recall = recall, prec = prec,
                       mu_star = mu_star, lam_star = lam_star,
                       tp_star = tp_star, fp_star = fp_star, tn_star = tn_star, fn_star = fn_star,
                       recall_star = recall_star, prec_star = prec_star)
  
  # mu = p(m | U, B), lam = p(u | M, B), recall = p(m | M), precision = p(M | m) ####
  results$emp_mu = results$fp / (results$fp + results$tn)
  results$emp_lam = results$fn / (results$fn + results$tp)
  results$emp_recall = results$tp / (results$tp + results$fn)
  results$emp_prec = results$tp / (results$tp + results$fp)
  
  return(list(results = results, weights = df))
}


#' Plot the performance of the matching
#' 
#' Generates ROC curves and plots the accuracy of the weights and error estimates
#' @param resuts output of results from fsPerformance
#' @param weights weights output from fsPerformance
#' @param comp_cols column names for columns used in the matching
#' @keywords performance
#' @export
#' @examples 
#' library(RecordLinkage, quietly = T)
#' data("RLdata10000")
#' dat = RLdata10000
#' dat$fi = substr(dat$fname_c1, 1,1)
#' dat$li = substr(dat$lname_c1, 1,1)
#' block_cols = c("fi", "li")
#' comp_cols = c("fname_c1" ,"lname_c1" ,"by" , "bm", "bd")
#' processed = makePairsDedup(dat, comp_cols, block_cols = block_cols, ids = identity.RLdata10000)
#' counts = processed$counts
#' des = processed$design
#' piM = 0.95^rowSums(des != "0")*0.05^(ncol(des)- rowSums(des != "0"))
#' piM = piM/sum(piM)
#' em = fsEM(des = processed$design, counts = processed$counts, 
#'          piM = (counts)/sum(counts), pM = 0.1, maxiter = 10000, tol = 1e-6)
#' performance = fsPerformance(pairs = processed$pairs, weights = em$weights, 
#'                             match_col = "is_match", comp_cols = comp_cols, pM = em$pM)
#' plots = plotResults(performance$results, performance$weights, comp_cols)

plotResults = function(results, weights, comp_cols){
  #args
  # Results no blocking, no threshold ######
  library(ggplot2)
  library(dplyr)
  # B := the pairs match on first letter of first and last names
  res = results
  df = weights
  
  # mu = p(m | U, B), lam = p(u | M, B), recall = p(m | M), precision = p(M | m) ####
  
  res$emp_mu_star = res$fp_star / (res$fp_star + res$tn_star)
  res$emp_lam_star = res$fn_star / (res$fn_star + res$tp_star)
  res$emp_recall_star = res$tp_star / (res$tp_star + res$fn_star)
  res$emp_prec_star = res$tp_star / (res$tp_star + res$fp_star)
  
  # How well did we estimate piM, piU?
  # Transform difficult comparisions into easier versions
  df1 = df
  for(i in 1:length(comp_cols)){
    name  = comp_cols[i]
    if(length(unique(df1[[name]])) > 2){
      vals = df %>% group_by_(name) %>% summarise(mean_w = mean(w))
      vals = vals[order(vals$mean_w), ]
      gaps = rep(NA, nrow(vals) - 1)
      for(j in 1:length(gaps)){
        gaps[j] = vals$mean_w[j + 1] - vals$mean_w[j]
      }
      ind = which.max(gaps)
      high_group = vals[[name]][1:length(vals[[name]]) > ind]
      df1[[name]] = as.numeric(df1[[name]] %in% high_group)
    }
  }
  # The weighted average of the weights for each bigger group
  df1$m_freq = df1$piM*df1$Freq
  df1$u_freq = df1$piU*df1$Freq
  df1$m_star_freq = df1$piMActual*df1$Freq
  df1$u_star_freq = df1$piUActual*df1$Freq
  dots = lapply(comp_cols, as.symbol)
  df1 = df1 %>% group_by_(.dots = dots) %>% summarise(piM = sum(m_freq)/sum(Freq),
                                                      piMActual =  sum(m_star_freq)/sum(Freq), 
                                                      piU = sum(u_freq)/sum(Freq),
                                                      piUActual =  sum(u_star_freq)/sum(Freq))
  ############### HHHHEEEEEERRRREEEEEEE ######
  
  # create a new data.frame = df
  # Replace multi-value columns in new df with their shorter versions
  # Sort by w
  # Break at the largest w gap
  # Make a binary piece of data
  df1$comparison = factor(apply( df1[ , comp_cols ] , 1 , paste , collapse = " , " ))
  xlab = paste(c("Comparison Value:", comp_cols), collapse = " , ") %>% gsub(": ,", ": ", .)
  pMPlot = ggplot(data = df1, mapping = aes(x = comparison, y = piM - piMActual)) +
    geom_bar(stat = "identity") + xlab(xlab) +
    theme(axis.text.x = element_text(angle = 90)) + ylab("Difference (Estimated - True)") +
    ggtitle("Estimated vs True P( g | M )")
  
  pUPlot = ggplot(data = df1, mapping = aes(x = comparison, y = piU - piUActual)) +
    geom_bar(stat = "identity") + xlab(xlab) +
    theme(axis.text.x = element_text(angle = 90)) + ylab("Difference (Estimated - True)") +
    ggtitle("Estimated vs True P( g | M )")
  
  
  # How well do the empiracal values match the predicted values? #####
  muPlot = ggplot(data = res, mapping = aes(x = mu, y = emp_mu)) + geom_point() + 
    geom_abline(slope = 1, lty = 2 ) + xlab(expression(paste("Predicted  ", mu))) +
    ylab(expression(paste("Empirical  ",mu))) + xlim(0,1) + ylim(0,1) +
    ggtitle(expression(paste("Empirical vs Predicted  ", mu)))
  
  lamPlot = ggplot(data = res, mapping = aes(x = lam, y = emp_lam)) + geom_point() + 
    geom_abline(slope = 1, lty = 2 ) + xlab(expression(paste("Predicted  ", lambda))) +
    ylab(expression(paste("Empirical  ",lambda))) + xlim(0,1) + ylim(0,1) +
    ggtitle(expression(paste("Empirical vs Predicted  ", lambda)))
  
  recallPlot =ggplot(data = res, mapping = aes(x = recall, y = emp_recall)) + geom_point() + 
    geom_abline(slope = 1, lty = 2 ) + xlab(paste("Predicted", expression(mu))) +
    xlab(paste("Empirical", expression(mu))) +
    ggtitle(paste("Predicted vs Empirical", expression(mu)))
  
  precPlot = ggplot(data = res, mapping = aes(x = prec, y = emp_prec)) + geom_point() + 
    geom_abline(slope = 1, lty = 2 ) + xlab(paste("Predicted", expression(mu))) +
    xlab(paste("Empirical", expression(mu))) +
    ggtitle(paste("Predicted vs Empirical", expression(mu)))
  
  ROC = ggplot(data = res, mapping = aes(x = 1- emp_prec, y = emp_recall)) + geom_point() + 
    geom_abline(slope = 1, lty = 2 ) + xlab("1 - Precision") +
    ylab("Recall") +
    ggtitle("Recall vs 1 - Precision")
  
  ROC_star = ROC + geom_line() +  geom_line(mapping = aes(x =  1- emp_prec_star, y = emp_recall_star, color = "red"))+
    geom_point(mapping = aes(x =  1- emp_prec_star, y = emp_recall_star, color = "red"))
  
  return(list(ROC_star = ROC_star, ROC = ROC, precPlot = precPlot, recallPlot = recallPlot, 
              lamPlot = lamPlot, muPlot = muPlot, pMPlot = pMPlot, pUPlot = pUPlot))
}

