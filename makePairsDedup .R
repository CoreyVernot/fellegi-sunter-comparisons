
#' Generating the pairs and initial comparison vectors
#'
#' This function generates the pairs of records and the initial comparison vectors for these pairs.
#' @param data The data to be paired
#' @param comp_cols Character vector of the columns to use in comparisons
#' @param block_cols Character vector of columns to use in blocking
#' @param jw_cols Character vector of columns to generate jaro-winkler comparisons for
#' @param ids Vector of the true identities of each row of the data. NA if ids are not available
#' @keywords pairs
#' @export
#' @examples
#' library(RecordLinkage, quietly = T)
#' data("RLdata10000")
#' dat = RLdata10000
#' dat$fi = substr(dat$fname_c1, 1,1)
#' dat$li = substr(dat$lname_c1, 1,1)
#' block_cols = c("fi", "li")
#' comp_cols = c("fname_c1" ,"lname_c1" ,"by" , "bm", "bd")
#' pairs = pairsDedup(dat, comp_cols, block_cols = block_cols, ids = NA)
pairsDedup = function(data, comp_cols, block_cols = c(), jw_cols = c(), ids = NA){
  #ATTN: Need to replace compare.dedup
  #ATTM: Need to create analogous function for linkage (this is deduplication)
  if(length(intersect(block_cols, comp_cols)) != 0){
    stop("comp_cols cannot be block_cols")
  }
  
  dat = cbind(data[comp_cols], data[block_cols]) # Blocking columns and comparison columns
  blockfld = match(block_cols, names(dat)) # blockfld are the indices of the block columns
  strcmp = match(jw_cols, names(dat)) # strcmp are the indices of the columns to have JW comparisons
  
  #Create pairs blocked on strcmp columns
  if(length(jw_cols) == 0){
    if(length(block_cols) == 0){
      dedup = compare.dedup(dat, identity = ids)
    }else{
      dedup = compare.dedup(dat, blockfld = blockfld, exclude = blockfld, identity = ids)
    }
  }else{
    if(length(block_cols) == 0){
      dedup = compare.dedup(dat, strcmp = strcmp, identity = ids)
    }else{
      dedup = compare.dedup(dat, blockfld = blockfld, exclude = blockfld, strcmp = strcmp, identity = ids)
    }
  }
  
  pairs = dedup$pairs
  return(pairs)
}

#' Adding jaro-winkler based groups to pairs
#'
#' This function descretizes the jaro-winkler scores for the pairs
#' @param pairs Pairs data frame output from pairsDedup
#' @param jw_cols columns to discretize by jaro-winkler score
#' @param group_jw Do you want to group by jaro-winkler score? Otherwise, does cutoff
#' @param jw_groups Vector of values of the jaro-winkler scores to break at
#' @param jw_cut Cutoff for binary jaro-winkler values. Used if group_jw = F
#' @keywords pairs
#' @export
#' @examples
#' library(RecordLinkage, quietly = T)
#' data("RLdata10000")
#' dat = RLdata10000
#' dat$fi = substr(dat$fname_c1, 1,1)
#' dat$li = substr(dat$lname_c1, 1,1)
#' block_cols = c("fi", "li")
#' comp_cols = c("fname_c1" ,"lname_c1" ,"by" , "bm", "bd")
#' pairs = pairsDedup(dat, comp_cols, block_cols = block_cols, ids = NA)
#' pairs = jwGrouping()
# Note - if jw_cols are specified and there aren't jw_groups, it automatically creates a binary around jw_cut
jwGrouping = function(pairs, jw_cols = c(), group_jw = F, jw_groups, jw_cut = jw_cut){
  if(group_jw & (length(jw_cols) == 0)){
    stop("There must be jw_cols specified for group_jw = T")
  }
  
  if(length(jw_cols) != 0 & (!group_jw)){
    for(col in jw_cols){
      pairs[[col]] = as.numeric(pairs[[col]] >= jw_cut) 
    }
  }
  
  if(group_jw){
    for(col in jw_cols){
      a = rep(NA, nrow(pairs))
      for(cut in jw_groups){a[pairs[[col]] >= cut] <- cut}
      pairs[[col]] = factor(a)
    }
  }
  return(pairs)
}

#' Adding frequency-based groupings to pairs
#'
#' This function descretizes the frequency of the feature value for the pairs
#' @param data Original data from which pairs were derived
#' @param pairs Pairs data frame output from pairsDedup
#' @param freq_cols Columns to group based on frequency
#' @param freq_groups Frequency cutoffs for grouping
#' @keywords pairs
#' @export
#' @examples
#' 1 + 1 #This is a placeholder for now
freqGrouping = function(data, pairs, freq_groups, freq_cols){
  if(freq_groups){
    freqs = list(rep(NA, length(freq_cols))) #list of dataframes of frequencies
    for(i in 1:length(freq_cols)){
      col = freq_cols[i] #column name
      #Data frame for the frequencies of each of the values of the column
      tab = table(data[[col]]) 
      d = data.frame(names(tab), as.numeric(tab))
      names(d) = c(col, paste("frequency_", col, sep = ""))
      #Defining frequency groups
      #TODO: http://kevinmeurer.com/a-simple-guide-to-entropy-based-discretization/
      d[[paste("freq_bin", col, sep = "_")]] = 
        as.character(cut(d[[2]], 3, include.lowest = TRUE, labels = c("low_freq", "med_freq", "high_freq")))
      freqs[[i]] = d
      
      # merge in the names to the pairs
      #df = data.frame(data[[col]], id = identity)
      df = data.frame(data[[col]], id = 1:nrow(data))
      names(df) = c(paste(col, 1, sep = "_"), "id1")
      pairs = merge(pairs, df,  by = "id1" , all.x = T)
      
      #df = data.frame(data[[col]], id = identity)
      df = data.frame(data[[col]], id = 1:nrow(data))
      names(df) = c(paste(col, 2, sep = "_"), "id2")
      pairs = merge(pairs, df,  by = "id2", all.x = T )
      
      pairs = merge(pairs, d, by.x = paste(col,1, sep = "_"), by.y = col)
      
      #grouping pairs that match on freq_cols by frequency of value
      labels = c( paste(col, 1, sep = "_" ), paste(col, 2, sep = "_"))
      same = pairs[[labels[1]]] == pairs[[labels[2]]]
      pairs[[col]] = as.character(pairs[[col]])
      pairs[[col]][same] = pairs[[paste("freq_bin", col, sep = "_")]][same]
      
    }
    
    #names(freqs) = freq_cols
    
    
  }
  return(pairs)
}

#' Generating the pairs and design matrix for the comparison vectors
#'
#' This function generates the pairs of records design matrix for pairs.
#' @param data The data to be paired
#' @param comp_cols Character vector of the columns to use in comparisons
#' @param block_cols Character vector of columns to use in blocking
#' @param jw_cols Character vector of columns to generate jaro-winkler comparisons for
#' @param ids Vector of the true identities of each row of the data. NA if ids are not available
#' @param group_jw Do you want to group by jaro-winkler score? Otherwise, does cutoff
#' @param jw_groups Vector of values of the jaro-winkler scores to break at
#' @param jw_cut Cutoff for binary jaro-winkler values. Used if group_jw = F
#' @param freq_cols Columns to group based on frequency
#' @param freq_groups Frequency cutoffs for grouping
#' @keywords pairs
#' @export
#' @examples
#' 
#' library(RecordLinkage, quietly = T)
#' data("RLdata10000")
#' dat = RLdata10000
#' dat$fi = substr(dat$fname_c1, 1,1)
#' dat$li = substr(dat$lname_c1, 1,1)
#' block_cols = c("fi", "li")
#' comp_cols = c("fname_c1" ,"lname_c1" ,"by" , "bm", "bd")
#' processed = makePairsDedup(dat, comp_cols, block_cols = block_cols, ids = NA)
makePairsDedup = function(data, comp_cols, block_cols = c(), jw_cols = c(), group_jw = F, jw_groups, jw_cut = 1, ids, 
                          freq_groups = F, freq_cols = c() ){
  print(length(jw_cols) == 0)
  print(group_jw)
  print("making pairs")
  pairs = pairsDedup(data, comp_cols = comp_cols, block_cols = block_cols, jw_cols = jw_cols, ids = identity)
  print("grouping pairs")
  pairs = jwGrouping(pairs, jw_cols = jw_cols, group_jw = group_jw, jw_groups = jw_groups, jw_cut= jw_cut)
  pairs = freqGrouping(data, pairs, freq_groups, freq_cols)
  return(makeDesign(pairs, comp_cols))
}

