
# fsComparison
This is a repository for fsComparison, implementing the Fellegi-Sunter method for record linkage using a highly flexible comparison space. It includes functions for implementing record linkage, predicting the performance of sets of paramaters for the linkage process, optimizing the parameters based on a loss function, and evaluating the performance graphically with ROC curves. The linkage proccess used is an expansion of the Fellegi-Sunter method outlined in "A theory for record linkage" (Fellegi & Sunter, 1969).
http://courses.cs.washington.edu/courses/cse590q/04au/papers/Felligi69.pdf 


## Set up

###Install

The folder "fsComparison" contains the package. Download this folder and set your working directory to the location of this folder. Then run:


library(devtools)

library(roxygen2)

install("fsComparison")

###Dependencies

R Packages Used:

* stats
* Hmisc
* dplyr
* ggplot2

## How to use
For testing the initial setup, check that the documentation has ben loaded. Run:

?fsEM

For initial vignettes, see the folder "getting started" in this repository. It includes vignette examples of how to use some of the tools in the package. For more details, see the documentation

## Who do I talk to?

* Corey Vernot (primary contact)
* Rebecca Steorts (secondary contact)