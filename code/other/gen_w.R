gen_w <- function(comp_vals, freq = rep(1, length(comp_vals)), component_name, ea = .01, eb = .01, ea0 = .01, eb0 = .01, same_freq = T, f_a = NA, f_b = NA, f_ab = NA ){
  if((!same_freq) & is.na(f_a + f_b + f_ab)){cat("Warning: please supply frequencies for a, b and ab or set same_req = T ")} # turn this into a real warning
  if(same_freq){
    p_a = p_b = p_ab = freq/sum(freq)
  }else{
    p_a = f_a/sum(f_a)
    p_b = f_b/sum(f_b)
    p_ab = f_ab/sum(f_ab)
    }# Assumes we have outside information on frequnecy of names, and people are from same population
  compare_vals <- c(comp_vals, "missing", "disagree")
  m_vals = p_ab*(1 - ea - eb - ea0 - eb0) #Assumes we know the error rate and missing rate (missing rate is easy to compute, error rate of course is not)
  m_miss <- ea0 + eb0 #likelihood of missing value
  m_dis <- ea + eb #likelihood of disagreement in name | Matching
  m <- c(m_vals, m_miss, m_dis)
  
  u_vals <- p_a*p_b*(1 - ea - eb - ea0 - eb0)
  u_miss <- ea + eb
  u_dis <- (1-(1-ea - eb)*sum(p_a*p_b))*(1- ea0 - eb0 )
  u <- c(u_vals , u_miss , u_dis)
  w <- log(m/u)
  
  comp_table<- data.frame(comp = compare_vals, w = w, m = m , u = u ) # have calculated the weights for every possible value of c_va, the name component of the comparison space
  comp_table$component <- component_name
  return(comp_table)
}
