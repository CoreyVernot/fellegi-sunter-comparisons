#Varying FS paramaters

source('~/Record Linkage Comparison/code/FSmethod/FSalgorithm.R', chdir = TRUE)
source('~/Record Linkage Comparison/code/FSmethod/functions.R', chdir = TRUE)
source('~/Record Linkage Comparison/code/FSmethod/inputData.R', chdir = TRUE)

inputData(2000, 1000, 1000, seed = 105)

#TODO: Run FSalgorithm with inputData
comp_cols <- colnames(sample_a)[colnames(sample_a) != "id"]
block  <- c("bm", "by")
matching <- FSalgorithm(file_a = sample_a, file_b = sample_b, mu = .00005, lamda = .001,
                       block = block, comp_cols = comp_cols, quietly = F, nsample = 500000, use_review = F, bound = "false_positive")

FSdata <- as.data.frame(matching$data)

mus <- c(.0001, .00005)
lamdas = c(.001, .0005)
nsamples = seq(300000, 500000, 100000)
pop_sizes = c(400, 600, 800)

parameters <- list(mus = mus, lamdas = lamdas, nsamples = nsamples, pop_sizes = pop_sizes)
parameters <- expand.grid(parameters)
p <- parameters
for(i in 1:nrow(parameters)){
  print(i)
  psize <- p$pop_sizes[i]
  inputData(psize, psize/2, psize/2, seed = 100)
  comp_cols <- colnames(sample_a)[colnames(sample_a) != "id"]
  block  <- c("bm", "by")
  matching <- FSalgorithm(file_a = sample_a, file_b = sample_b, mu = p$mus[i], lamda = p$lamdas[i],
                         block = block, comp_cols = comp_cols, quietly = F, nsample = p$nsamples[i])
  
  FSdata <- rbind(FSdata, as.data.frame(matching$data))
  save(FSdata, file = "FSdata_8_22.R")
}

#Error in if (w <= tlam) { : missing value where TRUE/FALSE needed



for(i in c(1, 20:30)){
  print(rep(i, 20))
  psize <- p$pop_sizes[i]
  inputData(psize, psize/2, psize/2, seed = 100)
  comp_cols <- colnames(sample_a)[colnames(sample_a) != "id"]
  block  <- c("bm", "by")
  matching <- FSalgorithm(file_a = sample_a, file_b = sample_b, mu = p$mus[i], lamda = p$lamdas[i],
                         block = block, comp_cols = comp_cols, quietly = F, nsample = p$nsamples[i])
  
  FSdata <- rbind(FSdata, as.data.frame(matching$data))
  save(FSdata, file = "FSdata_8_23.R")
}



