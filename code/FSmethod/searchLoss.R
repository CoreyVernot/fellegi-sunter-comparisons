#' Generating a List of Lists of parameters
#'
#' This function generates a list of lists of paramaters for the predictPerform and searchLoss functions
#' 
#' @keywords paramaters
#' @export
#' @examples
genParams = function(){}


#' Predicting the Linkage Performance
#'
#' This function predicts the linkage performance for each set in an input list of linkage parameters
#' @param params A list of lists of paramaters for the linkage proccess. 
#' @keywords performance
#' @export
#' @examples
#'library(RecordLinkage, quietly = T)
#'library(dplyr, quietly = T)
#'library(ggplot2, quietly = T)
#'data("RLdata10000")
#'dat = RLdata10000
#'dat = dat[c("fname_c1" ,"lname_c1" ,"by" , "bm", "bd")]
#'dat$fi = substr(dat$fname_c1, 1,1)
#' dat$li = substr(dat$lname_c1, 1,1)
#' p1 = list("comp_cols" = c("fname_c1" ,"lname_c1" ,"by" , "bm", "bd"),
#' "block_cols" = c("fi", "li"), "freq_cols" = c(), "freq_groups" = F , "jw_cols" =  c(),
#' "group_jw" = F, "jw_groups" = c(), "p.agree.match" = .95 , "pM" = .1,
#' "maxiter" = 10000, "tol" = 1e-6, "ids" = NA)
#'  
#' p2 = p1
#' p2$jw_cols = c("fname_c1", "lname_c1")
#' p2$jw_groups = c(0, .7, .8, .9, 1)
#'     
#' l = predictPerform(params, dat)
#' 
#' 
#' 
# Runs the EM algorithm over the data for each paramater list in the list of lists param
# Uses the p(m), p(u), u(g), m(g) data from the parameters to estimate the 
# False positives (fp), False negatives (fn), precision and the recall of
# Each set of parameters. Returns a list of dataframes, where each dataframe is for
# a set of paramaters, and each row represents the cutoff weight. The number of rows is
# the number of possible comparisons in the comparison space based on the parameters.

predictPerform = function(params, data){
  performList = as.list(rep(NA, length(params)))
  for(i in 1:length(params)){
    print(paste("paramater set", i, "of", length(params)))
    p = params[[i]]
    print(paste("before processed: group_jw = ", p$group_jw))
    processed = makePairsDedup(data, comp_cols = p$comp_cols, jw_cols = p$jw_cols, group_jw = p$group_jw, 
                               jw_groups = p$jw_groups, jw_cut = p$jw_cut, ids = p$ids)
    pairs = processed$pairs
    counts = processed$counts
    des = processed$design
    
    # initial values for conditional probabilities p(g | U), p(g | M)
    piU = (counts)/sum(counts) # When there are zero counts, may want to add 2, but when there are not, we don't need to
    piU = piU/sum(piU)
    piM = p$p.agree.match^rowSums(des != "0")*(1-p$p.agree.match)^(5- rowSums(des != "0"))
    piM = piM/sum(piM)
    
    print("em algorithm")
    em = fsEM(des, counts = counts, piM, piU , pM = p$pM, maxiter = p$maxiter, tol = p$tol)
    weights = em$weights
    pM = em$pM
    mu = lam = recall = prec = rep(NA, nrow(weights))
    weights = weights[order(weights$w), ]
    #Calculate the values of the performance measures - 
    print("calculating performance")
    for(i in 1:nrow(weights)){
      space_u = i >= 1:nrow(weights)
      space_m = 1:nrow(weights) > i
      lam[i] = sum(weights$piM[space_u])
      mu[i] = sum(weights$piU[space_m])
      recall[i] = sum(weights$piM[space_m])
      prec[i] = recall[i]*pM/(recall[i]*pM + mu[i]*(1-pM))
    }
    print(nrow(weights))
    print(length(lam))
    print(length(mu))
    print(length(prec))
    print(length(recall))
    print(length(lam*pM*sum(counts)))
    print(length(mu*(1-pM)*sum(counts)))
    
    df = data.frame(weights, lam = lam, mu =mu, prec = prec, recall = recall, fp = mu*(1-pM)*sum(counts), fn = lam*pM*sum(counts))
    performList[i] = list(pars = p, performance = df)
  }
  return(performList)
}

#' Calculate the loss based on a loss function
#'
#' This function calculates the loss based on a loss function, number of false positives, and nubmer of false negatives
#' @param lossFN a string of r code that when run would calculate the loss function
#' @param fn Number of false negatives
#' @param fp Number of false positives
#' @keywords paramaters
#' @export
#' @examples
#' lossFN = "fp + 2*fn"
#' calcLoss(lossFN, 20, 15)
#' calcLoss(lossFN, 15, 20)

calcLoss = function(lossFN, fn, fp){
  do = paste("loss = ", lossFN, sep = "")
  eval(parse(text= do)) 
  return(loss)
}

#' Finds parameters with smallest loss
#'
#' This function calculates searches through a list of lists of parameters and predicts the loss for each based on an input loss function
#' @param lossFN a string of r code that when run would calculate the loss function
#' @param params A list of lists of paramaters for the linkage proccess. 
#' @param data The data bein used in the matching proccess
#' @keywords paramaters
#' @export
#' @examples
#' 
searchLoss = function(lossFN, params, data){
  l = predictPerform(params, data)
  toReturn = as.list(rep(NA, length(l)))
  for(i in 1:length(l)){
    perf = l[[i]][[2]]
    pars = l[[i]][[1]]
    perf$loss = NA
    for(j in 1:nrow(perf)){
      perf$loss[j] = calcLoss(lossFN, fn = perf$fn[j], fp = perf$fp[j])
    }
    threshold = perf$w[perf$loss = min(perf$loss)]
    perf$match = as.numeric(perf$w > threshold)
    add = list("weights" = perf, loss = min(perf$loss), params = params)
    toReturn[i] = add
  }
  return(toReturn)
}
